# foo Gitlab

- Kind Cluster

- https://github.com/quickbooks2018/kind-nginx-ingress/blob/master/kind-amazonlinux.sh

- https://github.com/quickbooks2018/kind-nginx-ingress/blob/master/kind-config-ingress.sh


- Jenkins Setup in Kind Cluster
```bash
helm repo add jenkins https://charts.jenkins.io
helm repo update
helm repo ls
helm search repo jenkins
helm search repo jenkins/jenkins --versions
helm show values jenkins/jenkins --version 4.3.20
mkdir -p jenkins/values
helm show values jenkins/jenkins --version 4.3.20 > jenkins/values/jenkins.yaml
helm upgrade --install jenkins --namespace jenkins --create-namespace jenkins/jenkins --version 4.3.20 -f jenkins/values/jenkins.yaml --wait


# Set up port forwarding to the Jenkins UI from Cloud Shell
kubectl -n jenkins port-forward svc/jenkins --address 0.0.0.0 8090:8080

# Jenkins Secrets

kubectl get secrets -n jenkins 

kubectl get secrets/jenkins -n jenkins -o yaml

jenkins-admin-password: b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==
jenkins-admin-user: YWRtaW4=

echo -n 'b1NuY1NpWmdnR25ZemtuNWx5Mnl0NQ==' | base64 -d
oSncSiZggGnYzkn5ly2yt5
```

### This is a simple example of how to use kaniko to build a docker image in a gitlab pipeline.

- kaniko is a tool to build container images from a Dockerfile, inside a container or Kubernetes cluster.

- https://github.com/GoogleContainerTools/kaniko


- Deploy Tokens

```bash
jenkins
zga5wZCjaKG_vEKp2btw
```

- Repo Access Token   (REPO_CREDS_ID) ( I used this)
```bash
kaniko
glpat-HhsJvxEH6RwRTyHYN3H1
```
- docker login gitlab
```bash
docker login registry.gitlab.com
```

### Gitlab Registry login & Push Build Images
- Note: this flag is removed from below --docker-email=quickbooks2018@gmail.com 
```bash
kubectl --namespace jenkins create secret docker-registry foo-gitlab-credentials \
  --docker-server=registry.gitlab.com \
  --docker-username=jenkins \
  --docker-password=zga5wZCjaKG_vEKp2btw
```

- Repo Download with https
```bash
git clone https://oauth2:glpat-HhsJvxEH6RwRTyHYN3H1@gitlab.com/quickbooks2018/foo.git
```

- JenkinsfileCI

```bash
pipeline {
  agent {
    kubernetes {
      yaml """
kind: Pod
metadata:
 namespace: jenkins
spec:
  containers:
  - name: cloudgeeks-ci
    image: python:slim
    command:
    - cat
    tty: true
  - name: kaniko
    image: gcr.io/kaniko-project/executor:debug
    imagePullPolicy: Always
    command:
    - sleep
    args:
    - 9999999
    volumeMounts:
      - name: jenkins-docker-cfg
        mountPath: /kaniko/.docker
  volumes:
  - name: jenkins-docker-cfg
    projected:
      sources:
      - secret:
          name: gitlab-credentials
          items:
            - key: .dockerconfigjson
              path: config.json
"""
    }
  }
  stages {
    stage('Build foo with Kaniko') {
      steps {
        script {
          container('cloudgeeks-ci') {
            withCredentials([string(credentialsId: 'REPO_CREDS_ID', variable: 'REPO_PRIVATE_TOKEN')]) {
              sh '''
                set -e
                echo "Login to CI Repo..."
                echo "Build number is ${BUILD_NUMBER}"
                apt update -y && apt install -y git
                git clone https://oauth2:${REPO_PRIVATE_TOKEN}@gitlab.com/quickbooks2018/foo.git
                ls
              '''
            }
          }
          container(name: 'kaniko', shell: '/busybox/sh') {
            sh '''
              set -e
              ls
              pwd
              echo build number is "$BUILD_NUMBER"
              ls
              cd kaniko
              ls
              /kaniko/executor --context `pwd` --dockerfile=Dockerfile  --destination registry.gitlab.com/quickbooks2018/foo:${BUILD_NUMBER}
            '''
          }
        }
      }
    }
  }
}
```

### Gitlab Registry Pull Image Secret Accesss Token
- username: hello
- password: glpat-7MjfY7jXYrueuQxK77Rx

```bash
kubectl create ns hello
kubectl --namespace hello create secret docker-registry gitlab-image-pull-access \
  --docker-server=registry.gitlab.com \
  --docker-username=hello \
  --docker-password=glpat-7MjfY7jXYrueuQxK77Rx
```

- Jenkins Service Account Role Binding with application namespace foo which has default service account with name "default"
```bash
kubectl create rolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=jenkins:default --namespace=foo-dev
kubectl create rolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=jenkins:default --namespace=foo-staging
kubectl create rolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=jenkins:default --namespace=foo-prod
```